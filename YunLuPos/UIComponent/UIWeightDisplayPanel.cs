﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YunLuPos.UIComponent
{
    public partial class UIWeightDisplayPanel : UserControl
    {
        private Double weight = 0;
        public UIWeightDisplayPanel()
        {
            InitializeComponent();
        }

        public void setWeight(String text)
        {
            if(text == null || text.Trim().Equals(""))
            {
                weight = 0;
            }else
            {
                weight = Double.Parse(text);
                weight = weight / 1000;
                this.weightLabel.Text = weight.ToString("F3");
            }
        }

        public Double getWeight()
        {
            return weight;
        }
    }
}
