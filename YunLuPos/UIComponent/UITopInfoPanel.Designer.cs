﻿namespace YunLuPos.UIComponent
{
    partial class UITopInfoPanel
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.uiPanel1 = new Sunny.UI.UIPanel();
            this.shopName = new Sunny.UI.UILabel();
            this.posCode = new Sunny.UI.UILabel();
            this.time = new Sunny.UI.UILabel();
            this.date = new Sunny.UI.UILabel();
            this.cashierName = new Sunny.UI.UILabel();
            this.cashierCode = new Sunny.UI.UILabel();
            this.orderNo = new Sunny.UI.UILabel();
            this.orderType = new Sunny.UI.UILabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.netDes = new Sunny.UI.UILabel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.uiLabel8 = new Sunny.UI.UILabel();
            this.uiLabel7 = new Sunny.UI.UILabel();
            this.uiLabel6 = new Sunny.UI.UILabel();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.uiPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // uiPanel1
            // 
            this.uiPanel1.Controls.Add(this.shopName);
            this.uiPanel1.Controls.Add(this.posCode);
            this.uiPanel1.Controls.Add(this.time);
            this.uiPanel1.Controls.Add(this.date);
            this.uiPanel1.Controls.Add(this.cashierName);
            this.uiPanel1.Controls.Add(this.cashierCode);
            this.uiPanel1.Controls.Add(this.orderNo);
            this.uiPanel1.Controls.Add(this.orderType);
            this.uiPanel1.Controls.Add(this.panel1);
            this.uiPanel1.Controls.Add(this.uiLabel8);
            this.uiPanel1.Controls.Add(this.uiLabel7);
            this.uiPanel1.Controls.Add(this.uiLabel6);
            this.uiPanel1.Controls.Add(this.uiLabel5);
            this.uiPanel1.Controls.Add(this.uiLabel4);
            this.uiPanel1.Controls.Add(this.uiLabel3);
            this.uiPanel1.Controls.Add(this.uiLabel2);
            this.uiPanel1.Controls.Add(this.uiLabel1);
            this.uiPanel1.Controls.Add(this.pictureBox1);
            this.uiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiPanel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPanel1.Location = new System.Drawing.Point(0, 0);
            this.uiPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanel1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(931, 66);
            this.uiPanel1.TabIndex = 0;
            this.uiPanel1.Text = null;
            // 
            // shopName
            // 
            this.shopName.BackColor = System.Drawing.Color.Transparent;
            this.shopName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.shopName.Location = new System.Drawing.Point(641, 36);
            this.shopName.Name = "shopName";
            this.shopName.Size = new System.Drawing.Size(162, 23);
            this.shopName.TabIndex = 17;
            this.shopName.Text = "{}";
            this.shopName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // posCode
            // 
            this.posCode.BackColor = System.Drawing.Color.Transparent;
            this.posCode.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.posCode.Location = new System.Drawing.Point(641, 9);
            this.posCode.Name = "posCode";
            this.posCode.Size = new System.Drawing.Size(84, 23);
            this.posCode.TabIndex = 16;
            this.posCode.Text = "{}";
            this.posCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // time
            // 
            this.time.BackColor = System.Drawing.Color.Transparent;
            this.time.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.time.Location = new System.Drawing.Point(495, 35);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(99, 23);
            this.time.TabIndex = 15;
            this.time.Text = "{}";
            this.time.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // date
            // 
            this.date.BackColor = System.Drawing.Color.Transparent;
            this.date.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.date.Location = new System.Drawing.Point(495, 9);
            this.date.Name = "date";
            this.date.Size = new System.Drawing.Size(99, 23);
            this.date.TabIndex = 14;
            this.date.Text = "{}";
            this.date.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cashierName
            // 
            this.cashierName.BackColor = System.Drawing.Color.Transparent;
            this.cashierName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cashierName.Location = new System.Drawing.Point(381, 36);
            this.cashierName.Name = "cashierName";
            this.cashierName.Size = new System.Drawing.Size(75, 23);
            this.cashierName.TabIndex = 13;
            this.cashierName.Text = "{}";
            this.cashierName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cashierCode
            // 
            this.cashierCode.BackColor = System.Drawing.Color.Transparent;
            this.cashierCode.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cashierCode.Location = new System.Drawing.Point(381, 9);
            this.cashierCode.Name = "cashierCode";
            this.cashierCode.Size = new System.Drawing.Size(75, 23);
            this.cashierCode.TabIndex = 12;
            this.cashierCode.Text = "{}";
            this.cashierCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // orderNo
            // 
            this.orderNo.BackColor = System.Drawing.Color.Transparent;
            this.orderNo.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.orderNo.Location = new System.Drawing.Point(186, 36);
            this.orderNo.Name = "orderNo";
            this.orderNo.Size = new System.Drawing.Size(152, 23);
            this.orderNo.TabIndex = 11;
            this.orderNo.Text = "{}";
            this.orderNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // orderType
            // 
            this.orderType.BackColor = System.Drawing.Color.Transparent;
            this.orderType.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.orderType.Location = new System.Drawing.Point(185, 13);
            this.orderType.Name = "orderType";
            this.orderType.Size = new System.Drawing.Size(119, 23);
            this.orderType.TabIndex = 10;
            this.orderType.Text = "{}";
            this.orderType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.netDes);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(885, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(46, 66);
            this.panel1.TabIndex = 9;
            // 
            // netDes
            // 
            this.netDes.BackColor = System.Drawing.Color.Transparent;
            this.netDes.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.netDes.Location = new System.Drawing.Point(4, 37);
            this.netDes.Name = "netDes";
            this.netDes.Size = new System.Drawing.Size(42, 23);
            this.netDes.TabIndex = 18;
            this.netDes.Text = "联机";
            this.netDes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImage = global::YunLuPos.Properties.Resources.net_alive;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(11, 9);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(25, 25);
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            // 
            // uiLabel8
            // 
            this.uiLabel8.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel8.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel8.Location = new System.Drawing.Point(599, 9);
            this.uiLabel8.Name = "uiLabel8";
            this.uiLabel8.Size = new System.Drawing.Size(47, 23);
            this.uiLabel8.TabIndex = 8;
            this.uiLabel8.Text = "终端:";
            this.uiLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel7
            // 
            this.uiLabel7.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel7.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel7.Location = new System.Drawing.Point(599, 36);
            this.uiLabel7.Name = "uiLabel7";
            this.uiLabel7.Size = new System.Drawing.Size(47, 23);
            this.uiLabel7.TabIndex = 7;
            this.uiLabel7.Text = "机构:";
            this.uiLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel6
            // 
            this.uiLabel6.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel6.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel6.Location = new System.Drawing.Point(457, 35);
            this.uiLabel6.Name = "uiLabel6";
            this.uiLabel6.Size = new System.Drawing.Size(46, 23);
            this.uiLabel6.TabIndex = 6;
            this.uiLabel6.Text = "时间:";
            this.uiLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel5
            // 
            this.uiLabel5.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel5.Location = new System.Drawing.Point(456, 9);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(46, 23);
            this.uiLabel5.TabIndex = 5;
            this.uiLabel5.Text = "日期:";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel4
            // 
            this.uiLabel4.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel4.Location = new System.Drawing.Point(339, 36);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(48, 23);
            this.uiLabel4.TabIndex = 4;
            this.uiLabel4.Text = "姓名:";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(339, 9);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(48, 23);
            this.uiLabel3.TabIndex = 3;
            this.uiLabel3.Text = "工号:";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(146, 35);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(47, 23);
            this.uiLabel2.TabIndex = 2;
            this.uiLabel2.Text = "单号:";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(146, 13);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(47, 23);
            this.uiLabel1.TabIndex = 1;
            this.uiLabel1.Text = "模式:";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::YunLuPos.Properties.Resources.fh;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(8, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(132, 36);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // UITopInfoPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.uiPanel1);
            this.Name = "UITopInfoPanel";
            this.Size = new System.Drawing.Size(931, 66);
            this.Load += new System.EventHandler(this.UITopInfoPanel_Load);
            this.uiPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel6;
        private Sunny.UI.UILabel uiLabel7;
        private Sunny.UI.UILabel uiLabel8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Sunny.UI.UILabel orderType;
        private Sunny.UI.UILabel orderNo;
        private Sunny.UI.UILabel cashierCode;
        private Sunny.UI.UILabel cashierName;
        private Sunny.UI.UILabel date;
        private Sunny.UI.UILabel time;
        private Sunny.UI.UILabel shopName;
        private Sunny.UI.UILabel posCode;
        private System.Windows.Forms.Timer timer1;
        private Sunny.UI.UILabel netDes;
        public Sunny.UI.UIPanel uiPanel1;
    }
}
