﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using YunLuPos.Utils;

namespace YunLuPos.Printer
{
    class ComPrinter : PrintBase
    {
        SerialPort serialPort = null;
        public string COM_PORT = "COM_PORT";
        public string COM_RATE = "COM_RATE";
        public string COM_BIT = "COM_BIT";
        public string COM_STOPBIT = "COM_STOPBIT";
        public string COM_CHECKBIT = "COM_CHECKBIT";

        public override bool initPrinter()
        {
            COM_PORT = InIUtil.ReadInIData(InIUtil.PrintSection,InIUtil.PrintComPort);
            COM_RATE = InIUtil.ReadInIData(InIUtil.PrintSection,InIUtil.PrintComRate);
            COM_BIT = InIUtil.ReadInIData(InIUtil.PrintSection, InIUtil.PrintComBit);
            COM_STOPBIT = InIUtil.ReadInIData(InIUtil.PrintSection, InIUtil.PrintComStopBit);
            COM_CHECKBIT = InIUtil.ReadInIData(InIUtil.PrintSection, InIUtil.PrintComCheckBit);
            Console.WriteLine(COM_PORT);
            //设置参数
            serialPort = new SerialPort();
            serialPort.PortName = COM_PORT;
            serialPort.BaudRate = Int32.Parse(COM_RATE); //串行波特率
            serialPort.DataBits = Int32.Parse(COM_BIT); //每个字节的标准数据位长度
            serialPort.StopBits = StopBits.One; //设置每个字节的标准停止位数
            if ("1".Equals(COM_STOPBIT))
            {
                serialPort.StopBits = StopBits.One;
            }
            else if ("1.5".Equals(COM_STOPBIT))
            {
                serialPort.StopBits = StopBits.OnePointFive;

            }
            else if ("2".Equals(COM_STOPBIT))
            {
                serialPort.StopBits = StopBits.Two;
            }
            serialPort.Parity = Parity.None; //设置奇偶校验检查协议
            if ("Even".Equals(COM_CHECKBIT))
            {
                serialPort.Parity = Parity.Even;
            }
            else if ("Mark".Equals(COM_CHECKBIT))
            {
                serialPort.Parity = Parity.Mark;
            }
            else if ("Odd".Equals(COM_CHECKBIT))
            {
                serialPort.Parity = Parity.Odd;
            }
            else if ("Space".Equals(COM_CHECKBIT))
            {
                serialPort.Parity = Parity.Space;
            }
            return true;
        }

        public override bool write(byte[] b)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Write(b, 0, b.Length);
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool Dispose()
        {
            try
            {
                if (serialPort != null)
                {
                    serialPort.Close();
                }
            }
            catch { }
            return !serialPort.IsOpen;
        }

        public override bool open()
        {
            try
            {
                if (serialPort != null)
                {
                    serialPort.Open();
                }
            }
            catch { }
            return serialPort.IsOpen;
        }

        public override bool NewRow()
        {
            byte[] temp = new byte[] { 0x0A };
            return write(temp);
        }
    }
}
