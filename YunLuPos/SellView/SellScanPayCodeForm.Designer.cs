﻿namespace YunLuPos.SellView
{
    partial class SellScanPayCodeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.orderCode = new Sunny.UI.UILabel();
            this.amountLabel = new Sunny.UI.UILabel();
            this.message = new Sunny.UI.UILabel();
            this.authCode = new Sunny.UI.UITextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.progress = new Sunny.UI.UIProgressIndicator();
            this.payWorker = new System.ComponentModel.BackgroundWorker();
            this.selectButton = new Sunny.UI.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(10, 55);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(67, 23);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "流水号:";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(10, 99);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(67, 23);
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.Text = "金   额:";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(10, 142);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(67, 23);
            this.uiLabel3.TabIndex = 2;
            this.uiLabel3.Text = "付款码:";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // orderCode
            // 
            this.orderCode.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.orderCode.Location = new System.Drawing.Point(68, 55);
            this.orderCode.Name = "orderCode";
            this.orderCode.Size = new System.Drawing.Size(249, 23);
            this.orderCode.TabIndex = 4;
            this.orderCode.Text = "000000";
            this.orderCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // amountLabel
            // 
            this.amountLabel.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.amountLabel.Location = new System.Drawing.Point(68, 99);
            this.amountLabel.Name = "amountLabel";
            this.amountLabel.Size = new System.Drawing.Size(361, 23);
            this.amountLabel.TabIndex = 5;
            this.amountLabel.Text = "0.00";
            this.amountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // message
            // 
            this.message.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.message.Location = new System.Drawing.Point(73, 172);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(289, 82);
            this.message.TabIndex = 7;
            this.message.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // authCode
            // 
            this.authCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.authCode.FillColor = System.Drawing.Color.White;
            this.authCode.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.authCode.Location = new System.Drawing.Point(72, 140);
            this.authCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.authCode.Maximum = 2147483647D;
            this.authCode.Minimum = -2147483648D;
            this.authCode.MinimumSize = new System.Drawing.Size(1, 1);
            this.authCode.Name = "authCode";
            this.authCode.Padding = new System.Windows.Forms.Padding(5);
            this.authCode.Size = new System.Drawing.Size(357, 29);
            this.authCode.TabIndex = 8;
            this.authCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.authCode_KeyPress);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::YunLuPos.Properties.Resources.zfb;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(401, 224);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(32, 32);
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::YunLuPos.Properties.Resources.wx;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(368, 225);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 30);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // progress
            // 
            this.progress.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.progress.Location = new System.Drawing.Point(12, 187);
            this.progress.MinimumSize = new System.Drawing.Size(1, 1);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(55, 55);
            this.progress.TabIndex = 11;
            this.progress.Text = "uiProgressIndicator1";
            // 
            // payWorker
            // 
            this.payWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.payWorker_DoWork);
            this.payWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.payWorker_RunWorkerCompleted);
            // 
            // selectButton
            // 
            this.selectButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.selectButton.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.selectButton.Location = new System.Drawing.Point(327, 53);
            this.selectButton.MinimumSize = new System.Drawing.Size(1, 1);
            this.selectButton.Name = "selectButton";
            this.selectButton.Size = new System.Drawing.Size(102, 27);
            this.selectButton.TabIndex = 12;
            this.selectButton.Text = "F2查询支付结果";
            this.selectButton.Click += new System.EventHandler(this.selectButton_Click);
            // 
            // SellScanPayCodeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 264);
            this.Controls.Add(this.selectButton);
            this.Controls.Add(this.progress);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.authCode);
            this.Controls.Add(this.message);
            this.Controls.Add(this.amountLabel);
            this.Controls.Add(this.orderCode);
            this.Controls.Add(this.uiLabel3);
            this.Controls.Add(this.uiLabel2);
            this.Controls.Add(this.uiLabel1);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SellScanPayCodeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "请扫电子付款码";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SellScanPayCodeForm_FormClosing);
            this.Load += new System.EventHandler(this.SellScanPayCodeForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SellScanPayCodeForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UILabel orderCode;
        private Sunny.UI.UILabel amountLabel;
        private Sunny.UI.UILabel message;
        private Sunny.UI.UITextBox authCode;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Sunny.UI.UIProgressIndicator progress;
        private System.ComponentModel.BackgroundWorker payWorker;
        private Sunny.UI.UIButton selectButton;
    }
}