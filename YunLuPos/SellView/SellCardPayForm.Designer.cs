﻿namespace YunLuPos.SellView
{
    partial class SellCardPayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.selectButton = new Sunny.UI.UIButton();
            this.progress = new Sunny.UI.UIProgressIndicator();
            this.cardNo = new Sunny.UI.UITextBox();
            this.amountLabel = new Sunny.UI.UILabel();
            this.orderCode = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.message = new Sunny.UI.UILabel();
            this.cardPwd = new Sunny.UI.UITextBox();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.payWorker = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // selectButton
            // 
            this.selectButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.selectButton.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.selectButton.Location = new System.Drawing.Point(328, 44);
            this.selectButton.MinimumSize = new System.Drawing.Size(1, 1);
            this.selectButton.Name = "selectButton";
            this.selectButton.Size = new System.Drawing.Size(102, 27);
            this.selectButton.TabIndex = 20;
            this.selectButton.Text = "F2查询支付结果";
            this.selectButton.Click += new System.EventHandler(this.selectButton_Click);
            // 
            // progress
            // 
            this.progress.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.progress.Location = new System.Drawing.Point(15, 208);
            this.progress.MinimumSize = new System.Drawing.Size(1, 1);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(55, 55);
            this.progress.TabIndex = 19;
            this.progress.Text = "uiProgressIndicator1";
            // 
            // cardNo
            // 
            this.cardNo.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.cardNo.FillColor = System.Drawing.Color.White;
            this.cardNo.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cardNo.Location = new System.Drawing.Point(73, 118);
            this.cardNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cardNo.Maximum = 2147483647D;
            this.cardNo.Minimum = -2147483648D;
            this.cardNo.MinimumSize = new System.Drawing.Size(1, 1);
            this.cardNo.Name = "cardNo";
            this.cardNo.Padding = new System.Windows.Forms.Padding(5);
            this.cardNo.Size = new System.Drawing.Size(357, 29);
            this.cardNo.TabIndex = 18;
            this.cardNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cardNo_KeyPress);
            // 
            // amountLabel
            // 
            this.amountLabel.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.amountLabel.Location = new System.Drawing.Point(69, 79);
            this.amountLabel.Name = "amountLabel";
            this.amountLabel.Size = new System.Drawing.Size(361, 23);
            this.amountLabel.TabIndex = 17;
            this.amountLabel.Text = "0.00";
            this.amountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // orderCode
            // 
            this.orderCode.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.orderCode.Location = new System.Drawing.Point(69, 46);
            this.orderCode.Name = "orderCode";
            this.orderCode.Size = new System.Drawing.Size(249, 23);
            this.orderCode.TabIndex = 16;
            this.orderCode.Text = "000000";
            this.orderCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(11, 120);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(67, 23);
            this.uiLabel3.TabIndex = 15;
            this.uiLabel3.Text = "卡   号:";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(11, 79);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(67, 23);
            this.uiLabel2.TabIndex = 14;
            this.uiLabel2.Text = "金   额:";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(11, 46);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(67, 23);
            this.uiLabel1.TabIndex = 13;
            this.uiLabel1.Text = "流水号:";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // message
            // 
            this.message.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.message.Location = new System.Drawing.Point(74, 208);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(356, 58);
            this.message.TabIndex = 21;
            this.message.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cardPwd
            // 
            this.cardPwd.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.cardPwd.FillColor = System.Drawing.Color.White;
            this.cardPwd.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cardPwd.Location = new System.Drawing.Point(73, 159);
            this.cardPwd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cardPwd.Maximum = 2147483647D;
            this.cardPwd.Minimum = -2147483648D;
            this.cardPwd.MinimumSize = new System.Drawing.Size(1, 1);
            this.cardPwd.Name = "cardPwd";
            this.cardPwd.Padding = new System.Windows.Forms.Padding(5);
            this.cardPwd.PasswordChar = '*';
            this.cardPwd.Size = new System.Drawing.Size(357, 29);
            this.cardPwd.TabIndex = 20;
            this.cardPwd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cardPwd_KeyPress);
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel4.Location = new System.Drawing.Point(11, 161);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(67, 23);
            this.uiLabel4.TabIndex = 19;
            this.uiLabel4.Text = "密   码:";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // payWorker
            // 
            this.payWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.payWorker_DoWork);
            this.payWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.payWorker_RunWorkerCompleted);
            // 
            // SellCardPayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 279);
            this.Controls.Add(this.cardPwd);
            this.Controls.Add(this.uiLabel4);
            this.Controls.Add(this.message);
            this.Controls.Add(this.selectButton);
            this.Controls.Add(this.progress);
            this.Controls.Add(this.cardNo);
            this.Controls.Add(this.amountLabel);
            this.Controls.Add(this.orderCode);
            this.Controls.Add(this.uiLabel3);
            this.Controls.Add(this.uiLabel2);
            this.Controls.Add(this.uiLabel1);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SellCardPayForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "请刷卡或输入储值卡号";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SellCardPayForm_FormClosing);
            this.Load += new System.EventHandler(this.SellCardPayForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SellCardPayForm_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIButton selectButton;
        private Sunny.UI.UIProgressIndicator progress;
        private Sunny.UI.UITextBox cardNo;
        private Sunny.UI.UILabel amountLabel;
        private Sunny.UI.UILabel orderCode;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel message;
        private Sunny.UI.UITextBox cardPwd;
        private Sunny.UI.UILabel uiLabel4;
        private System.ComponentModel.BackgroundWorker payWorker;
    }
}