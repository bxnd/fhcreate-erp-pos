﻿namespace YunLuPos.SellView
{
    partial class SellPayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            this.uiPanel1 = new Sunny.UI.UIPanel();
            this.payTypeGrid = new Sunny.UI.UIDataGridView();
            this.payType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uiPanel2 = new Sunny.UI.UIPanel();
            this.changeAmount = new Sunny.UI.UILabel();
            this.message = new Sunny.UI.UILabel();
            this.lessAmount = new Sunny.UI.UILabel();
            this.payedAmount = new Sunny.UI.UILabel();
            this.payAmount = new Sunny.UI.UILabel();
            this.amountInput = new Sunny.UI.UITextBox();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiPanel3 = new Sunny.UI.UIPanel();
            this.payedGrid = new Sunny.UI.UIDataGridView();
            this.typeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uiPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.payTypeGrid)).BeginInit();
            this.uiPanel2.SuspendLayout();
            this.uiPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.payedGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlBtm
            // 
            this.pnlBtm.Location = new System.Drawing.Point(1, 366);
            this.pnlBtm.Size = new System.Drawing.Size(621, 55);
            // 
            // uiPanel1
            // 
            this.uiPanel1.Controls.Add(this.payTypeGrid);
            this.uiPanel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPanel1.Location = new System.Drawing.Point(5, 42);
            this.uiPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanel1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(157, 316);
            this.uiPanel1.TabIndex = 2;
            this.uiPanel1.Text = null;
            // 
            // payTypeGrid
            // 
            this.payTypeGrid.AllowUserToAddRows = false;
            this.payTypeGrid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.payTypeGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.payTypeGrid.BackgroundColor = System.Drawing.Color.White;
            this.payTypeGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.payTypeGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.payTypeGrid.ColumnHeadersHeight = 32;
            this.payTypeGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.payTypeGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.payType});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.payTypeGrid.DefaultCellStyle = dataGridViewCellStyle13;
            this.payTypeGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.payTypeGrid.EnableHeadersVisualStyles = false;
            this.payTypeGrid.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.payTypeGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.payTypeGrid.Location = new System.Drawing.Point(0, 0);
            this.payTypeGrid.Name = "payTypeGrid";
            this.payTypeGrid.ReadOnly = true;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.payTypeGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            this.payTypeGrid.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.payTypeGrid.RowTemplate.Height = 29;
            this.payTypeGrid.SelectedIndex = -1;
            this.payTypeGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.payTypeGrid.ShowGridLine = true;
            this.payTypeGrid.Size = new System.Drawing.Size(157, 316);
            this.payTypeGrid.TabIndex = 0;
            this.payTypeGrid.CurrentCellChanged += new System.EventHandler(this.payTypeGrid_CurrentCellChanged);
            // 
            // payType
            // 
            this.payType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.payType.DataPropertyName = "payTypeName";
            this.payType.HeaderText = "支付方式";
            this.payType.Name = "payType";
            this.payType.ReadOnly = true;
            // 
            // uiPanel2
            // 
            this.uiPanel2.Controls.Add(this.changeAmount);
            this.uiPanel2.Controls.Add(this.message);
            this.uiPanel2.Controls.Add(this.lessAmount);
            this.uiPanel2.Controls.Add(this.payedAmount);
            this.uiPanel2.Controls.Add(this.payAmount);
            this.uiPanel2.Controls.Add(this.amountInput);
            this.uiPanel2.Controls.Add(this.uiLabel5);
            this.uiPanel2.Controls.Add(this.uiLabel4);
            this.uiPanel2.Controls.Add(this.uiLabel3);
            this.uiPanel2.Controls.Add(this.uiLabel2);
            this.uiPanel2.Controls.Add(this.uiLabel1);
            this.uiPanel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPanel2.Location = new System.Drawing.Point(167, 44);
            this.uiPanel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanel2.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanel2.Name = "uiPanel2";
            this.uiPanel2.Size = new System.Drawing.Size(448, 102);
            this.uiPanel2.TabIndex = 3;
            this.uiPanel2.Text = null;
            // 
            // changeAmount
            // 
            this.changeAmount.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.changeAmount.Location = new System.Drawing.Point(209, 43);
            this.changeAmount.Name = "changeAmount";
            this.changeAmount.Size = new System.Drawing.Size(146, 23);
            this.changeAmount.TabIndex = 10;
            this.changeAmount.Text = "0.00";
            this.changeAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // message
            // 
            this.message.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.message.Location = new System.Drawing.Point(62, 74);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(362, 23);
            this.message.TabIndex = 9;
            this.message.Text = "*";
            this.message.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lessAmount
            // 
            this.lessAmount.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lessAmount.Location = new System.Drawing.Point(348, 8);
            this.lessAmount.Name = "lessAmount";
            this.lessAmount.Size = new System.Drawing.Size(98, 23);
            this.lessAmount.TabIndex = 8;
            this.lessAmount.Text = "0.00";
            this.lessAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // payedAmount
            // 
            this.payedAmount.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.payedAmount.Location = new System.Drawing.Point(209, 8);
            this.payedAmount.Name = "payedAmount";
            this.payedAmount.Size = new System.Drawing.Size(100, 23);
            this.payedAmount.TabIndex = 7;
            this.payedAmount.Text = "0.00";
            this.payedAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // payAmount
            // 
            this.payAmount.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.payAmount.Location = new System.Drawing.Point(54, 8);
            this.payAmount.Name = "payAmount";
            this.payAmount.Size = new System.Drawing.Size(107, 23);
            this.payAmount.TabIndex = 6;
            this.payAmount.Text = "0.00";
            this.payAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // amountInput
            // 
            this.amountInput.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.amountInput.FillColor = System.Drawing.Color.White;
            this.amountInput.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.amountInput.Location = new System.Drawing.Point(14, 40);
            this.amountInput.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.amountInput.Maximum = 2147483647D;
            this.amountInput.Minimum = -2147483648D;
            this.amountInput.MinimumSize = new System.Drawing.Size(1, 1);
            this.amountInput.Name = "amountInput";
            this.amountInput.Padding = new System.Windows.Forms.Padding(5);
            this.amountInput.Size = new System.Drawing.Size(146, 29);
            this.amountInput.TabIndex = 5;
            this.amountInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.amountInput_KeyPress);
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel5.Location = new System.Drawing.Point(10, 73);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(46, 23);
            this.uiLabel5.TabIndex = 4;
            this.uiLabel5.Text = "消息:";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel4.Location = new System.Drawing.Point(167, 43);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(46, 23);
            this.uiLabel4.TabIndex = 3;
            this.uiLabel4.Text = "找零:";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(307, 8);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(48, 23);
            this.uiLabel3.TabIndex = 2;
            this.uiLabel3.Text = "差异:";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(167, 8);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(46, 23);
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.Text = "已收:";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(10, 8);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(47, 23);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "应收:";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiPanel3
            // 
            this.uiPanel3.Controls.Add(this.payedGrid);
            this.uiPanel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPanel3.Location = new System.Drawing.Point(167, 154);
            this.uiPanel3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanel3.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanel3.Name = "uiPanel3";
            this.uiPanel3.Size = new System.Drawing.Size(448, 204);
            this.uiPanel3.TabIndex = 4;
            this.uiPanel3.Text = null;
            // 
            // payedGrid
            // 
            this.payedGrid.AllowUserToAddRows = false;
            this.payedGrid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.payedGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            this.payedGrid.BackgroundColor = System.Drawing.Color.White;
            this.payedGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle17.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.payedGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.payedGrid.ColumnHeadersHeight = 32;
            this.payedGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.payedGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.typeName,
            this.amount});
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.payedGrid.DefaultCellStyle = dataGridViewCellStyle18;
            this.payedGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.payedGrid.EnableHeadersVisualStyles = false;
            this.payedGrid.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.payedGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.payedGrid.Location = new System.Drawing.Point(0, 0);
            this.payedGrid.Name = "payedGrid";
            this.payedGrid.ReadOnly = true;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.payedGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle19;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.White;
            this.payedGrid.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this.payedGrid.RowTemplate.Height = 29;
            this.payedGrid.SelectedIndex = -1;
            this.payedGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.payedGrid.ShowGridLine = true;
            this.payedGrid.Size = new System.Drawing.Size(448, 204);
            this.payedGrid.TabIndex = 0;
            // 
            // typeName
            // 
            this.typeName.DataPropertyName = "typeName";
            this.typeName.FillWeight = 200F;
            this.typeName.HeaderText = "支付方式";
            this.typeName.Name = "typeName";
            this.typeName.ReadOnly = true;
            this.typeName.Width = 150;
            // 
            // amount
            // 
            this.amount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.amount.DataPropertyName = "amount";
            this.amount.HeaderText = "金额";
            this.amount.Name = "amount";
            this.amount.ReadOnly = true;
            // 
            // SellPayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 424);
            this.Controls.Add(this.uiPanel3);
            this.Controls.Add(this.uiPanel2);
            this.Controls.Add(this.uiPanel1);
            this.KeyPreview = true;
            this.Name = "SellPayForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "收银结算";
            this.Load += new System.EventHandler(this.SellPayForm_Load);
            this.Shown += new System.EventHandler(this.SellPayForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SellPayForm_KeyDown);
            this.Controls.SetChildIndex(this.pnlBtm, 0);
            this.Controls.SetChildIndex(this.uiPanel1, 0);
            this.Controls.SetChildIndex(this.uiPanel2, 0);
            this.Controls.SetChildIndex(this.uiPanel3, 0);
            this.uiPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.payTypeGrid)).EndInit();
            this.uiPanel2.ResumeLayout(false);
            this.uiPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.payedGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIPanel uiPanel1;
        private Sunny.UI.UIPanel uiPanel2;
        private Sunny.UI.UIPanel uiPanel3;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UITextBox amountInput;
        private Sunny.UI.UILabel payAmount;
        private Sunny.UI.UILabel payedAmount;
        private Sunny.UI.UILabel lessAmount;
        private Sunny.UI.UILabel message;
        private Sunny.UI.UILabel changeAmount;
        private Sunny.UI.UIDataGridView payTypeGrid;
        private Sunny.UI.UIDataGridView payedGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn payType;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn amount;
    }
}