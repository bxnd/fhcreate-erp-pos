﻿namespace YunLuPos.View
{
    partial class PayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            this.shortBar1 = new YunLuPos.Com.ShortBar();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.message = new YunLuPos.Com.BaseLabel(this.components);
            this.baseButton2 = new YunLuPos.Com.BaseButton(this.components);
            this.baseButton1 = new YunLuPos.Com.BaseButton(this.components);
            this.changeAmount = new YunLuPos.Com.BaseLabel(this.components);
            this.lessAmount = new YunLuPos.Com.BaseLabel(this.components);
            this.payedAmount = new YunLuPos.Com.BaseLabel(this.components);
            this.payAmount = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel4 = new YunLuPos.Com.BaseLabel(this.components);
            this.amountInput = new YunLuPos.Com.BaseTextInput(this.components);
            this.baseLabel3 = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel2 = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel1 = new YunLuPos.Com.BaseLabel(this.components);
            this.payedGrid = new YunLuPos.Com.BaseGrid(this.components);
            this.typeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.payTypeGrid = new YunLuPos.Com.BaseGrid(this.components);
            this.payName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.payedGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.payTypeGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Size = new System.Drawing.Size(701, 41);
            this.label1.Text = "结算";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(2, 2);
            this.panel1.Size = new System.Drawing.Size(701, 451);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.shortBar1);
            this.panel3.Size = new System.Drawing.Size(701, 410);
            // 
            // shortBar1
            // 
            this.shortBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.shortBar1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.shortBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.shortBar1.Location = new System.Drawing.Point(0, 0);
            this.shortBar1.Name = "shortBar1";
            this.shortBar1.Size = new System.Drawing.Size(90, 410);
            this.shortBar1.TabIndex = 0;
            this.shortBar1.Visible = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.payTypeGrid);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(90, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(611, 410);
            this.panel4.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.message);
            this.panel5.Controls.Add(this.baseButton2);
            this.panel5.Controls.Add(this.baseButton1);
            this.panel5.Controls.Add(this.changeAmount);
            this.panel5.Controls.Add(this.lessAmount);
            this.panel5.Controls.Add(this.payedAmount);
            this.panel5.Controls.Add(this.payAmount);
            this.panel5.Controls.Add(this.baseLabel4);
            this.panel5.Controls.Add(this.amountInput);
            this.panel5.Controls.Add(this.baseLabel3);
            this.panel5.Controls.Add(this.baseLabel2);
            this.panel5.Controls.Add(this.baseLabel1);
            this.panel5.Controls.Add(this.payedGrid);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(150, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(461, 410);
            this.panel5.TabIndex = 1;
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.ForeColor = System.Drawing.Color.Gold;
            this.message.Location = new System.Drawing.Point(8, 78);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(11, 12);
            this.message.TabIndex = 14;
            this.message.Text = "*";
            // 
            // baseButton2
            // 
            this.baseButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.baseButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton2.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton2.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton2.ForeColor = System.Drawing.Color.White;
            this.baseButton2.Location = new System.Drawing.Point(372, 369);
            this.baseButton2.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton2.Name = "baseButton2";
            this.baseButton2.Size = new System.Drawing.Size(80, 30);
            this.baseButton2.TabIndex = 13;
            this.baseButton2.Text = "Esc取消";
            this.baseButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton2.UseVisualStyleBackColor = false;
            this.baseButton2.Click += new System.EventHandler(this.baseButton2_Click);
            // 
            // baseButton1
            // 
            this.baseButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.baseButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton1.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton1.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton1.ForeColor = System.Drawing.Color.White;
            this.baseButton1.Location = new System.Drawing.Point(289, 369);
            this.baseButton1.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton1.Name = "baseButton1";
            this.baseButton1.Size = new System.Drawing.Size(80, 30);
            this.baseButton1.TabIndex = 12;
            this.baseButton1.Text = "Enter确认";
            this.baseButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton1.UseVisualStyleBackColor = false;
            this.baseButton1.Click += new System.EventHandler(this.baseButton1_Click);
            // 
            // changeAmount
            // 
            this.changeAmount.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeAmount.ForeColor = System.Drawing.Color.White;
            this.changeAmount.Location = new System.Drawing.Point(183, 46);
            this.changeAmount.Name = "changeAmount";
            this.changeAmount.Size = new System.Drawing.Size(80, 23);
            this.changeAmount.TabIndex = 11;
            this.changeAmount.Text = "0.00";
            // 
            // lessAmount
            // 
            this.lessAmount.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lessAmount.ForeColor = System.Drawing.Color.White;
            this.lessAmount.Location = new System.Drawing.Point(309, 7);
            this.lessAmount.Name = "lessAmount";
            this.lessAmount.Size = new System.Drawing.Size(80, 23);
            this.lessAmount.TabIndex = 10;
            this.lessAmount.Text = "0.00";
            // 
            // payedAmount
            // 
            this.payedAmount.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.payedAmount.ForeColor = System.Drawing.Color.White;
            this.payedAmount.Location = new System.Drawing.Point(181, 7);
            this.payedAmount.Name = "payedAmount";
            this.payedAmount.Size = new System.Drawing.Size(80, 23);
            this.payedAmount.TabIndex = 9;
            this.payedAmount.Text = "0.00";
            // 
            // payAmount
            // 
            this.payAmount.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.payAmount.ForeColor = System.Drawing.Color.Gold;
            this.payAmount.Location = new System.Drawing.Point(39, 7);
            this.payAmount.Name = "payAmount";
            this.payAmount.Size = new System.Drawing.Size(80, 23);
            this.payAmount.TabIndex = 8;
            this.payAmount.Text = "0.00";
            // 
            // baseLabel4
            // 
            this.baseLabel4.AutoSize = true;
            this.baseLabel4.ForeColor = System.Drawing.Color.White;
            this.baseLabel4.Location = new System.Drawing.Point(150, 50);
            this.baseLabel4.Name = "baseLabel4";
            this.baseLabel4.Size = new System.Drawing.Size(35, 12);
            this.baseLabel4.TabIndex = 7;
            this.baseLabel4.Text = "找零:";
            // 
            // amountInput
            // 
            this.amountInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.amountInput.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amountInput.Location = new System.Drawing.Point(7, 43);
            this.amountInput.Name = "amountInput";
            this.amountInput.Size = new System.Drawing.Size(111, 26);
            this.amountInput.TabIndex = 6;
            this.amountInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.amountInput_KeyPress);
            // 
            // baseLabel3
            // 
            this.baseLabel3.AutoSize = true;
            this.baseLabel3.ForeColor = System.Drawing.Color.White;
            this.baseLabel3.Location = new System.Drawing.Point(277, 14);
            this.baseLabel3.Name = "baseLabel3";
            this.baseLabel3.Size = new System.Drawing.Size(35, 12);
            this.baseLabel3.TabIndex = 5;
            this.baseLabel3.Text = "差值:";
            // 
            // baseLabel2
            // 
            this.baseLabel2.AutoSize = true;
            this.baseLabel2.ForeColor = System.Drawing.Color.White;
            this.baseLabel2.Location = new System.Drawing.Point(150, 14);
            this.baseLabel2.Name = "baseLabel2";
            this.baseLabel2.Size = new System.Drawing.Size(35, 12);
            this.baseLabel2.TabIndex = 4;
            this.baseLabel2.Text = "已收:";
            // 
            // baseLabel1
            // 
            this.baseLabel1.AutoSize = true;
            this.baseLabel1.ForeColor = System.Drawing.Color.White;
            this.baseLabel1.Location = new System.Drawing.Point(5, 14);
            this.baseLabel1.Name = "baseLabel1";
            this.baseLabel1.Size = new System.Drawing.Size(35, 12);
            this.baseLabel1.TabIndex = 3;
            this.baseLabel1.Text = "应收:";
            // 
            // payedGrid
            // 
            this.payedGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.payedGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.payedGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.payedGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.payedGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle25.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.payedGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.payedGrid.ColumnHeadersHeight = 25;
            this.payedGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.typeName,
            this.amount});
            this.payedGrid.CustomFoceColor = System.Drawing.Color.White;
            this.payedGrid.CustomMainColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle26.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.payedGrid.DefaultCellStyle = dataGridViewCellStyle26;
            this.payedGrid.Enabled = false;
            this.payedGrid.EnableHeadersVisualStyles = false;
            this.payedGrid.GridColor = System.Drawing.Color.White;
            this.payedGrid.Location = new System.Drawing.Point(3, 103);
            this.payedGrid.MultiSelect = false;
            this.payedGrid.Name = "payedGrid";
            this.payedGrid.ReadOnly = true;
            this.payedGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle27.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.payedGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.payedGrid.RowTemplate.Height = 25;
            this.payedGrid.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.payedGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.payedGrid.Size = new System.Drawing.Size(454, 251);
            this.payedGrid.TabIndex = 2;
            // 
            // typeName
            // 
            this.typeName.DataPropertyName = "typeName";
            this.typeName.HeaderText = "类型";
            this.typeName.Name = "typeName";
            this.typeName.ReadOnly = true;
            // 
            // amount
            // 
            this.amount.DataPropertyName = "amount";
            this.amount.HeaderText = "金额";
            this.amount.Name = "amount";
            this.amount.ReadOnly = true;
            // 
            // payTypeGrid
            // 
            this.payTypeGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.payTypeGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.payTypeGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.payTypeGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle28.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.payTypeGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle28;
            this.payTypeGrid.ColumnHeadersHeight = 25;
            this.payTypeGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.payName});
            this.payTypeGrid.CustomFoceColor = System.Drawing.Color.White;
            this.payTypeGrid.CustomMainColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle29.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.payTypeGrid.DefaultCellStyle = dataGridViewCellStyle29;
            this.payTypeGrid.Dock = System.Windows.Forms.DockStyle.Left;
            this.payTypeGrid.EnableHeadersVisualStyles = false;
            this.payTypeGrid.GridColor = System.Drawing.Color.White;
            this.payTypeGrid.Location = new System.Drawing.Point(0, 0);
            this.payTypeGrid.MultiSelect = false;
            this.payTypeGrid.Name = "payTypeGrid";
            this.payTypeGrid.ReadOnly = true;
            this.payTypeGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle30.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.payTypeGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.payTypeGrid.RowHeadersWidth = 30;
            this.payTypeGrid.RowTemplate.Height = 25;
            this.payTypeGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.payTypeGrid.Size = new System.Drawing.Size(150, 410);
            this.payTypeGrid.TabIndex = 0;
            this.payTypeGrid.CurrentCellChanged += new System.EventHandler(this.payTypeGrid_CurrentCellChanged);
            // 
            // payName
            // 
            this.payName.DataPropertyName = "payTypeName";
            this.payName.HeaderText = "支付方式";
            this.payName.Name = "payName";
            this.payName.ReadOnly = true;
            // 
            // PayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(705, 455);
            this.Name = "PayForm";
            this.Text = "PayForm";
            this.Load += new System.EventHandler(this.PayForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PayForm_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.payedGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.payTypeGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Com.ShortBar shortBar1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private Com.BaseButton baseButton2;
        private Com.BaseButton baseButton1;
        private Com.BaseLabel changeAmount;
        private Com.BaseLabel lessAmount;
        private Com.BaseLabel payedAmount;
        private Com.BaseLabel payAmount;
        private Com.BaseLabel baseLabel4;
        private Com.BaseTextInput amountInput;
        private Com.BaseLabel baseLabel3;
        private Com.BaseLabel baseLabel2;
        private Com.BaseLabel baseLabel1;
        private Com.BaseGrid payedGrid;
        private Com.BaseGrid payTypeGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn payName;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn amount;
        private Com.BaseLabel message;
    }
}