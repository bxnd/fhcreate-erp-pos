﻿namespace YunLuPos
{
    partial class LoginInitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginInitForm));
            this.downWorker = new System.ComponentModel.BackgroundWorker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.versionLabel = new YunLuPos.Com.BaseLabel(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.taskMessage = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel1 = new YunLuPos.Com.BaseLabel(this.components);
            this.pwdInput = new YunLuPos.Com.BaseTextInput(this.components);
            this.codeInput = new YunLuPos.Com.BaseTextInput(this.components);
            this.baseLabel13 = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel12 = new YunLuPos.Com.BaseLabel(this.components);
            this.panel5 = new System.Windows.Forms.Panel();
            this.baseLabel14 = new YunLuPos.Com.BaseLabel(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // downWorker
            // 
            this.downWorker.WorkerReportsProgress = true;
            this.downWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.downWorker_DoWork);
            this.downWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.downWorker_ProgressChanged);
            this.downWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.downWorker_RunWorkerCompleted);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::YunLuPos.Properties.Resources.loginbg;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.versionLabel);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(784, 562);
            this.panel1.TabIndex = 0;
            // 
            // versionLabel
            // 
            this.versionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.versionLabel.BackColor = System.Drawing.Color.Transparent;
            this.versionLabel.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.versionLabel.ForeColor = System.Drawing.Color.Gray;
            this.versionLabel.Location = new System.Drawing.Point(644, 520);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(128, 23);
            this.versionLabel.TabIndex = 11;
            this.versionLabel.Text = "版本：1.0";
            this.versionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(586, 529);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 14);
            this.label1.TabIndex = 10;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::YunLuPos.Properties.Resources.fh;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(138, 39);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.taskMessage);
            this.panel4.Controls.Add(this.baseLabel1);
            this.panel4.Controls.Add(this.pwdInput);
            this.panel4.Controls.Add(this.codeInput);
            this.panel4.Controls.Add(this.baseLabel13);
            this.panel4.Controls.Add(this.baseLabel12);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Location = new System.Drawing.Point(230, 150);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(356, 188);
            this.panel4.TabIndex = 2;
            // 
            // taskMessage
            // 
            this.taskMessage.AutoSize = true;
            this.taskMessage.ForeColor = System.Drawing.Color.White;
            this.taskMessage.Location = new System.Drawing.Point(84, 155);
            this.taskMessage.Name = "taskMessage";
            this.taskMessage.Size = new System.Drawing.Size(41, 12);
            this.taskMessage.TabIndex = 6;
            this.taskMessage.Text = "未开始";
            // 
            // baseLabel1
            // 
            this.baseLabel1.AutoSize = true;
            this.baseLabel1.ForeColor = System.Drawing.Color.White;
            this.baseLabel1.Location = new System.Drawing.Point(25, 155);
            this.baseLabel1.Name = "baseLabel1";
            this.baseLabel1.Size = new System.Drawing.Size(53, 12);
            this.baseLabel1.TabIndex = 5;
            this.baseLabel1.Text = "任  务：";
            // 
            // pwdInput
            // 
            this.pwdInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pwdInput.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pwdInput.Location = new System.Drawing.Point(84, 108);
            this.pwdInput.Name = "pwdInput";
            this.pwdInput.PasswordChar = '*';
            this.pwdInput.Size = new System.Drawing.Size(234, 26);
            this.pwdInput.TabIndex = 4;
            this.pwdInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.pwdInput_KeyPress);
            // 
            // codeInput
            // 
            this.codeInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.codeInput.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.codeInput.Location = new System.Drawing.Point(84, 64);
            this.codeInput.Name = "codeInput";
            this.codeInput.Size = new System.Drawing.Size(234, 26);
            this.codeInput.TabIndex = 3;
            this.codeInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.codeInput_KeyPress);
            // 
            // baseLabel13
            // 
            this.baseLabel13.AutoSize = true;
            this.baseLabel13.ForeColor = System.Drawing.Color.White;
            this.baseLabel13.Location = new System.Drawing.Point(25, 115);
            this.baseLabel13.Name = "baseLabel13";
            this.baseLabel13.Size = new System.Drawing.Size(53, 12);
            this.baseLabel13.TabIndex = 2;
            this.baseLabel13.Text = "密  码：";
            // 
            // baseLabel12
            // 
            this.baseLabel12.AutoSize = true;
            this.baseLabel12.ForeColor = System.Drawing.Color.White;
            this.baseLabel12.Location = new System.Drawing.Point(25, 71);
            this.baseLabel12.Name = "baseLabel12";
            this.baseLabel12.Size = new System.Drawing.Size(53, 12);
            this.baseLabel12.TabIndex = 1;
            this.baseLabel12.Text = "工  号：";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.baseLabel14);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(354, 38);
            this.panel5.TabIndex = 0;
            // 
            // baseLabel14
            // 
            this.baseLabel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.baseLabel14.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.baseLabel14.ForeColor = System.Drawing.Color.White;
            this.baseLabel14.Location = new System.Drawing.Point(0, 0);
            this.baseLabel14.Name = "baseLabel14";
            this.baseLabel14.Size = new System.Drawing.Size(352, 36);
            this.baseLabel14.TabIndex = 0;
            this.baseLabel14.Text = "富海云创收银台";
            this.baseLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LoginInitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoginInitForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "富海云创收银台";
            this.Load += new System.EventHandler(this.LoginInitForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private Com.BaseLabel baseLabel13;
        private Com.BaseLabel baseLabel12;
        private Com.BaseTextInput pwdInput;
        private Com.BaseTextInput codeInput;
        private Com.BaseLabel baseLabel1;
        private Com.BaseLabel taskMessage;
        private System.Windows.Forms.Panel panel5;
        private Com.BaseLabel baseLabel14;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private Com.BaseLabel versionLabel;
        private System.ComponentModel.BackgroundWorker downWorker;
    }
}