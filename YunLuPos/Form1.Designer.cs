﻿namespace YunLuPos
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.baseButton3 = new YunLuPos.Com.BaseButton(this.components);
            this.baseButton2 = new YunLuPos.Com.BaseButton(this.components);
            this.baseTextInput1 = new YunLuPos.Com.BaseTextInput(this.components);
            this.baseButton1 = new YunLuPos.Com.BaseButton(this.components);
            this.baseButton4 = new YunLuPos.Com.BaseButton(this.components);
            this.baseButton5 = new YunLuPos.Com.BaseButton(this.components);
            this.baseButton6 = new YunLuPos.Com.BaseButton(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(639, 189);
            this.dataGridView1.TabIndex = 5;
            // 
            // baseButton3
            // 
            this.baseButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton3.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton3.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton3.ForeColor = System.Drawing.Color.White;
            this.baseButton3.Location = new System.Drawing.Point(448, 280);
            this.baseButton3.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton3.Name = "baseButton3";
            this.baseButton3.Size = new System.Drawing.Size(90, 30);
            this.baseButton3.TabIndex = 4;
            this.baseButton3.Text = "获取";
            this.baseButton3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton3.UseVisualStyleBackColor = false;
            this.baseButton3.Click += new System.EventHandler(this.baseButton3_Click);
            // 
            // baseButton2
            // 
            this.baseButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton2.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton2.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton2.ForeColor = System.Drawing.Color.White;
            this.baseButton2.Location = new System.Drawing.Point(668, 234);
            this.baseButton2.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton2.Name = "baseButton2";
            this.baseButton2.Size = new System.Drawing.Size(90, 30);
            this.baseButton2.TabIndex = 3;
            this.baseButton2.Text = "生成实体";
            this.baseButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton2.UseVisualStyleBackColor = false;
            this.baseButton2.Click += new System.EventHandler(this.baseButton2_Click);
            // 
            // baseTextInput1
            // 
            this.baseTextInput1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.baseTextInput1.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.baseTextInput1.Location = new System.Drawing.Point(291, 378);
            this.baseTextInput1.Name = "baseTextInput1";
            this.baseTextInput1.Size = new System.Drawing.Size(145, 26);
            this.baseTextInput1.TabIndex = 2;
            // 
            // baseButton1
            // 
            this.baseButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton1.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton1.CustomMouseDownColor = System.Drawing.Color.Gold;
            this.baseButton1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gold;
            this.baseButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton1.ForeColor = System.Drawing.Color.White;
            this.baseButton1.Location = new System.Drawing.Point(670, 31);
            this.baseButton1.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton1.Name = "baseButton1";
            this.baseButton1.Size = new System.Drawing.Size(90, 30);
            this.baseButton1.TabIndex = 0;
            this.baseButton1.Text = "baseButton1";
            this.baseButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton1.UseVisualStyleBackColor = false;
            this.baseButton1.Click += new System.EventHandler(this.baseButton1_Click);
            // 
            // baseButton4
            // 
            this.baseButton4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton4.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton4.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton4.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton4.ForeColor = System.Drawing.Color.White;
            this.baseButton4.Location = new System.Drawing.Point(448, 328);
            this.baseButton4.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton4.Name = "baseButton4";
            this.baseButton4.Size = new System.Drawing.Size(90, 30);
            this.baseButton4.TabIndex = 6;
            this.baseButton4.Text = "请求";
            this.baseButton4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton4.UseVisualStyleBackColor = false;
            this.baseButton4.Click += new System.EventHandler(this.baseButton4_Click);
            // 
            // baseButton5
            // 
            this.baseButton5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton5.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton5.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton5.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton5.ForeColor = System.Drawing.Color.White;
            this.baseButton5.Location = new System.Drawing.Point(70, 244);
            this.baseButton5.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton5.Name = "baseButton5";
            this.baseButton5.Size = new System.Drawing.Size(90, 30);
            this.baseButton5.TabIndex = 7;
            this.baseButton5.Text = "baseButton5";
            this.baseButton5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton5.UseVisualStyleBackColor = false;
            this.baseButton5.Click += new System.EventHandler(this.baseButton5_Click);
            // 
            // baseButton6
            // 
            this.baseButton6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton6.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton6.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton6.ForeColor = System.Drawing.Color.White;
            this.baseButton6.Location = new System.Drawing.Point(247, 250);
            this.baseButton6.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton6.Name = "baseButton6";
            this.baseButton6.Size = new System.Drawing.Size(90, 30);
            this.baseButton6.TabIndex = 8;
            this.baseButton6.Text = "baseButton6";
            this.baseButton6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton6.UseVisualStyleBackColor = false;
            this.baseButton6.Click += new System.EventHandler(this.baseButton6_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(806, 452);
            this.Controls.Add(this.baseButton6);
            this.Controls.Add(this.baseButton5);
            this.Controls.Add(this.baseButton4);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.baseButton3);
            this.Controls.Add(this.baseButton2);
            this.Controls.Add(this.baseTextInput1);
            this.Controls.Add(this.baseButton1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Com.BaseButton baseButton1;
        private Com.BaseTextInput baseTextInput1;
        private Com.BaseButton baseButton2;
        private Com.BaseButton baseButton3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private Com.BaseButton baseButton4;
        private Com.BaseButton baseButton5;
        private Com.BaseButton baseButton6;
    }
}

