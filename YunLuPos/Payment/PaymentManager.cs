﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Entity;

namespace YunLuPos.Payment
{
    public class PaymentManager
    {
        static List<PaymentInterface> payServices = new List<PaymentInterface>();

        static PaymentManager(){
            payServices.Add(new CashPayment());
            payServices.Add(new ScanPayment());
            payServices.Add(new UnionScanPayment());
            payServices.Add(new ABCPayment());
            payServices.Add(new HisPayPayment());
            payServices.Add(new CardPayment());
            payServices.Add(new MemberPayment());
        }

        public static ResultMessage doPay(SaleOrder order, PayType payType, PaymentState state, Double inputAmount)
        {
            PaymentInterface pi = null;
            foreach(PaymentInterface p in payServices)
            {
                if (p.getKey().Equals(payType.serviceKey)){
                    pi = p;
                    break;
                }
            }
            if(pi == null)
            {
                return ResultMessage.createError("支付服务缺失！");
            }
            return pi.doPay(order, payType, state, inputAmount);
        }

        public static List<String> print(String payTypeKey)
        {
            PaymentInterface pi = null;
            foreach (PaymentInterface p in payServices)
            {
                if (p.getKey().Equals(payTypeKey))
                {
                    pi = p;
                    break;
                }
            }
            if (pi != null)
            {                
                return pi.print();
            }
            return null;
        }
    }
}
