﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Payment
{
    public class ABCPayResult
    {
        private String result;    //返回码

        private String authNum;  // 授权号
         
        private String cardNum; //银行卡号

        private String amount;  //金额

        private String systemNum; //系统参考号

        private String validity; //有效期

        private String payDate; //交易日期

        private String payTime; //交易时间

        private String resultDescribe;//结果描述

        private String tenantNum; //商户号

        private String driveNum;//终端号

        private String cardType; //内外卡标识

        private String bankNum; //银行流水号

        private String bankName; //银行名称

        private String sourceString;//

        public string Result
        {
            get
            {
                return result;
            }

            set
            {
                result = value;
            }
        }

        public string AuthNum
        {
            get
            {
                return authNum;
            }

            set
            {
                authNum = value;
            }
        }

        public string CardNum
        {
            get
            {
                return cardNum;
            }

            set
            {
                cardNum = value;
            }
        }

        public string Amount
        {
            get
            {
                return amount;
            }

            set
            {
                amount = value;
            }
        }

        public string SystemNum
        {
            get
            {
                return systemNum;
            }

            set
            {
                systemNum = value;
            }
        }

        public string Validity
        {
            get
            {
                return validity;
            }

            set
            {
                validity = value;
            }
        }

        public string PayDate
        {
            get
            {
                return payDate;
            }

            set
            {
                payDate = value;
            }
        }

        public string PayTime
        {
            get
            {
                return payTime;
            }

            set
            {
                payTime = value;
            }
        }

        public string ResultDescribe
        {
            get
            {
                return resultDescribe;
            }

            set
            {
                resultDescribe = value;
            }
        }

        public string TenantNum
        {
            get
            {
                return tenantNum;
            }

            set
            {
                tenantNum = value;
            }
        }

        public string DriveNum
        {
            get
            {
                return driveNum;
            }

            set
            {
                driveNum = value;
            }
        }

        public string CardType
        {
            get
            {
                return cardType;
            }

            set
            {
                cardType = value;
            }
        }

        public string BankNum
        {
            get
            {
                return bankNum;
            }

            set
            {
                bankNum = value;
            }
        }

        public string BankName
        {
            get
            {
                return bankName;
            }

            set
            {
                bankName = value;
            }
        }

        public string SourceString
        {
            get
            {
                return sourceString;
            }

            set
            {
                sourceString = value;
            }
        }
    }
}
