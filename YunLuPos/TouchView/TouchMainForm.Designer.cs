﻿namespace YunLuPos.TouchView
{
    partial class TouchMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.uiFlowLayoutPanel1 = new Sunny.UI.UIFlowLayoutPanel();
            this.uiButton1 = new Sunny.UI.UIButton();
            this.shortPanel = new Sunny.UI.UIPanel();
            this.uiDataGridView1 = new Sunny.UI.UIDataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uiWeightDisplayPanel1 = new YunLuPos.UIComponent.UIWeightDisplayPanel();
            this.paymentPanel = new YunLuPos.UIComponent.UIPaymentPanel();
            this.uiMemberInfoPanel1 = new YunLuPos.UIComponent.UIMemberInfoPanel();
            this.preOrderInfoPanel = new YunLuPos.UIComponent.UIPreOrderInfoPanel();
            this.goodsInfoPanel = new YunLuPos.UIComponent.UIGoodsInfoPanel();
            this.topInfoPanel = new YunLuPos.UIComponent.UITopInfoPanel();
            ((System.ComponentModel.ISupportInitialize)(this.uiDataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // uiFlowLayoutPanel1
            // 
            this.uiFlowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uiFlowLayoutPanel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiFlowLayoutPanel1.Location = new System.Drawing.Point(350, 101);
            this.uiFlowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiFlowLayoutPanel1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiFlowLayoutPanel1.Name = "uiFlowLayoutPanel1";
            this.uiFlowLayoutPanel1.Padding = new System.Windows.Forms.Padding(2);
            this.uiFlowLayoutPanel1.Size = new System.Drawing.Size(645, 486);
            this.uiFlowLayoutPanel1.TabIndex = 17;
            this.uiFlowLayoutPanel1.Text = "uiFlowLayoutPanel1";
            // 
            // uiButton1
            // 
            this.uiButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uiButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiButton1.Location = new System.Drawing.Point(5, 549);
            this.uiButton1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(230, 35);
            this.uiButton1.TabIndex = 18;
            this.uiButton1.Text = "结算";
            // 
            // shortPanel
            // 
            this.shortPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.shortPanel.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.shortPanel.Location = new System.Drawing.Point(239, 101);
            this.shortPanel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.shortPanel.MinimumSize = new System.Drawing.Size(1, 1);
            this.shortPanel.Name = "shortPanel";
            this.shortPanel.Size = new System.Drawing.Size(108, 486);
            this.shortPanel.TabIndex = 19;
            this.shortPanel.Text = null;
            // 
            // uiDataGridView1
            // 
            this.uiDataGridView1.AllowUserToAddRows = false;
            this.uiDataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.uiDataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.uiDataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.uiDataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.uiDataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.uiDataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.uiDataGridView1.ColumnHeadersHeight = 50;
            this.uiDataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.uiDataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            this.uiDataGridView1.EnableHeadersVisualStyles = false;
            this.uiDataGridView1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiDataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.uiDataGridView1.Location = new System.Drawing.Point(5, 101);
            this.uiDataGridView1.Name = "uiDataGridView1";
            this.uiDataGridView1.ReadOnly = true;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            this.uiDataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.uiDataGridView1.RowTemplate.Height = 50;
            this.uiDataGridView1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.uiDataGridView1.SelectedIndex = -1;
            this.uiDataGridView1.ShowGridLine = true;
            this.uiDataGridView1.Size = new System.Drawing.Size(230, 442);
            this.uiDataGridView1.Style = Sunny.UI.UIStyle.Custom;
            this.uiDataGridView1.TabIndex = 20;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.DataPropertyName = "caption";
            this.Column1.HeaderText = "品名   数量  价格";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // uiWeightDisplayPanel1
            // 
            this.uiWeightDisplayPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uiWeightDisplayPanel1.Location = new System.Drawing.Point(396, 589);
            this.uiWeightDisplayPanel1.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.uiWeightDisplayPanel1.Name = "uiWeightDisplayPanel1";
            this.uiWeightDisplayPanel1.Size = new System.Drawing.Size(137, 102);
            this.uiWeightDisplayPanel1.TabIndex = 15;
            // 
            // paymentPanel
            // 
            this.paymentPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.paymentPanel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.paymentPanel.Location = new System.Drawing.Point(842, 589);
            this.paymentPanel.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.paymentPanel.Name = "paymentPanel";
            this.paymentPanel.Size = new System.Drawing.Size(153, 102);
            this.paymentPanel.TabIndex = 14;
            // 
            // uiMemberInfoPanel1
            // 
            this.uiMemberInfoPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiMemberInfoPanel1.Location = new System.Drawing.Point(722, 589);
            this.uiMemberInfoPanel1.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.uiMemberInfoPanel1.Name = "uiMemberInfoPanel1";
            this.uiMemberInfoPanel1.Size = new System.Drawing.Size(115, 102);
            this.uiMemberInfoPanel1.TabIndex = 13;
            // 
            // preOrderInfoPanel
            // 
            this.preOrderInfoPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.preOrderInfoPanel.Location = new System.Drawing.Point(239, 589);
            this.preOrderInfoPanel.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.preOrderInfoPanel.Name = "preOrderInfoPanel";
            this.preOrderInfoPanel.Size = new System.Drawing.Size(153, 102);
            this.preOrderInfoPanel.TabIndex = 12;
            // 
            // goodsInfoPanel
            // 
            this.goodsInfoPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.goodsInfoPanel.Location = new System.Drawing.Point(5, 589);
            this.goodsInfoPanel.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.goodsInfoPanel.Name = "goodsInfoPanel";
            this.goodsInfoPanel.Size = new System.Drawing.Size(230, 102);
            this.goodsInfoPanel.TabIndex = 11;
            // 
            // topInfoPanel
            // 
            this.topInfoPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topInfoPanel.Location = new System.Drawing.Point(0, 35);
            this.topInfoPanel.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.topInfoPanel.Name = "topInfoPanel";
            this.topInfoPanel.Size = new System.Drawing.Size(1000, 64);
            this.topInfoPanel.TabIndex = 1;
            // 
            // TouchMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1000, 700);
            this.Controls.Add(this.uiDataGridView1);
            this.Controls.Add(this.shortPanel);
            this.Controls.Add(this.uiButton1);
            this.Controls.Add(this.uiFlowLayoutPanel1);
            this.Controls.Add(this.uiWeightDisplayPanel1);
            this.Controls.Add(this.paymentPanel);
            this.Controls.Add(this.uiMemberInfoPanel1);
            this.Controls.Add(this.preOrderInfoPanel);
            this.Controls.Add(this.goodsInfoPanel);
            this.Controls.Add(this.topInfoPanel);
            this.Name = "TouchMainForm";
            this.Text = "富海云创收银台";
            this.Load += new System.EventHandler(this.TouchMainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uiDataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private UIComponent.UITopInfoPanel topInfoPanel;
        public UIComponent.UIWeightDisplayPanel uiWeightDisplayPanel1;
        private UIComponent.UIPaymentPanel paymentPanel;
        private UIComponent.UIMemberInfoPanel uiMemberInfoPanel1;
        private UIComponent.UIPreOrderInfoPanel preOrderInfoPanel;
        public UIComponent.UIGoodsInfoPanel goodsInfoPanel;
        private Sunny.UI.UIFlowLayoutPanel uiFlowLayoutPanel1;
        private Sunny.UI.UIButton uiButton1;
        public Sunny.UI.UIPanel shortPanel;
        private Sunny.UI.UIDataGridView uiDataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
    }
}