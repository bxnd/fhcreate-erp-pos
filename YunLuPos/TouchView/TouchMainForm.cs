﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Command;
using YunLuPos.DB.Service;
using YunLuPos.Entity;

namespace YunLuPos.TouchView
{
    

    public partial class TouchMainForm : UIForm
    {
        GoodsService goodsService = new GoodsService();
        public TouchMainForm()
        {
            InitializeComponent();
        }

        private void TouchMainForm_Load(object sender, EventArgs e)
        {
            init();

            Dto d = new Dto();
            d.caption = "红旗渠蓝色经典   ------22343" + System.Environment.NewLine+"12312321";
            List<Dto> ds = new List<Dto>();
            ds.Add(d);
            this.uiDataGridView1.DataSource = ds;
            


            List<Goods> gs = goodsService.listTouchGoods();
            gs.ForEach(l => {
                
                    UIButton btn = new UIButton();
                    btn.Height = 80;
                    btn.Width = 130;
                    btn.Style = this.Style;
                    String name = l.goodsName;
                    btn.Font = new Font(btn.Font.FontFamily, 11);
                    if(name.Length > 10)
                    {
                        btn.Font = new Font(btn.Font.FontFamily, 10);
                    }
                    if (name.Length > 14)
                    {
                        btn.Font = new Font(btn.Font.FontFamily, 9);
                    }

                /*if(name.Length > 8)
                {
                    String line1 = name.Substring(0, 8);
                    Console.WriteLine(line1);
                    String line2 = name.Replace(line1, "");
                    Console.WriteLine(line2);
                    name = line1 +"\r\n" +line2;
                    btn.Font = new Font(btn.Font.FontFamily, 10);
                }*/

                btn.Text = name + "\r\n￥" + l.salePrice.ToString("F2") + "\r\n"+l.barCode;
                    btn.TextAlign = ContentAlignment.MiddleLeft;
                        
                btn.Click += new System.EventHandler(this.ShortPanelButtonClick);
                    btn.Tag = l.barCode;
                    Padding pad = new Padding(2, 0, 0, 5);
                    btn.Margin = pad;

                   


                    this.uiFlowLayoutPanel1.AddControl(btn);
                
            });
        }

        //快捷按钮点击事件
        private void ShortPanelButtonClick(object sender, EventArgs e)
        {
            UIButton btn = (UIButton)sender;
            String tag = (String)btn.Tag;
            this.ShowInfoDialog("1234123");
            
        }


        void init()
        {
            try
            {
                int w = Int32.Parse(StaticInfoHoder.Width);
                int h = Int32.Parse(StaticInfoHoder.Height);
                this.Width = w;
                this.Height = h;
                this.ShowTitle = (StaticInfoHoder.ShowTop == "1");
                if (StaticInfoHoder.FullScreen == "1")
                {
                    this.WindowState = FormWindowState.Maximized;
                }
                else
                {
                    this.WindowState = FormWindowState.Normal;
                }

            }
            catch
            {

            }
        }
    }

    class Dto
    {
        public String caption { get; set; }
    }
}
