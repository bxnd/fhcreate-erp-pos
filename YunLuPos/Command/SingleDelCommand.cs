﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Com;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.SellView;

namespace YunLuPos.Command
{
    class SingleDelCommand : CommandInterface
    {
        SaleOrderGoodsService orderGoodsService = new SaleOrderGoodsService();
        public void exec(Form handel, Cashier auther)
        {
            if (DynamicInfoHoder.currentOrder.goods == null || DynamicInfoHoder.currentOrder.goods.Count <= 0)
            {
                return;
            }
            MainForm mf = handel as MainForm;
            if (mf != null)
            {
                if(mf.goodsGrid.CurrentRow == null)
                {
                    return;
                }
                ConfirmDialog confirm = new ConfirmDialog("删除当前单品？");
                if (confirm.ShowDialog() == DialogResult.OK)
                {
                    SaleOrderGoods goods = mf.goodsGrid.CurrentRow.DataBoundItem as SaleOrderGoods;
                    if(goods != null)
                    {
                        Boolean b = orderGoodsService.delGoods(goods);
                        if (b)
                        {
                            mf.displayCurrentOrder(); //刷新当前单据界面显示
                        }

                    }
                }
            }


            SellMainForm smf = handel as SellMainForm;
            if (smf != null)
            {
                if (smf.goodsGrid.CurrentRow == null)
                {
                    return;
                }
                if (smf.ShowAskDialog("单品删除","确认删除选中单品?",smf.Style))
                {
                    SaleOrderGoods goods = smf.goodsGrid.CurrentRow.DataBoundItem as SaleOrderGoods;
                    if (goods != null)
                    {
                        Boolean b = orderGoodsService.delGoods(goods);
                        if (b)
                        {
                            smf.displayCurrentOrder(); //刷新当前单据界面显示
                        }

                    }
                }
            }
        }

        public string getKey()
        {
            return Names.SingleDel.ToString();
        }

        public bool needAuth()
        {
            return true;
        }
    }
}
