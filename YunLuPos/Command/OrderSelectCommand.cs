﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Entity;
using YunLuPos.SellView;

namespace YunLuPos.Command
{
    /**
     * 获取光标命令
     * */
    public class OrderSelectCommand : CommandInterface
    {
        public void exec(Form handel, Cashier cashier)
        {
            MainForm mf = handel as MainForm;
            if (mf != null)
            {
                mf.ActiveControl = mf.goodsInfoPanel1.codeInput;
            }

            SellMainForm smf = handel as SellMainForm;
            if (smf != null)
            {
                OrderSelectForm osf = new OrderSelectForm();
                osf.Style = smf.Style;
                if( osf.ShowDialog() == DialogResult.OK)
                {
                    //通过退货按钮关闭
                    smf.displayCurrentOrder();
                }
            }
        }

        public string getKey()
        {
            return Names.OrderSelect.ToString();
        }

        public bool needAuth()
        {
            return false;
        }
    }
}
