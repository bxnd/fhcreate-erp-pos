﻿using SQLiteSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Entity;

namespace YunLuPos.DB
{
    public class SugarDao
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(SugarDao));
        //禁止实例化
        private SugarDao()
        {

        }
        public static SqlSugarClient GetInstance()
        {

            var db = new SqlSugarClient(Constant.DB_FILE);
            //db.IsEnableLogEvent = true;//启用日志事件
            db.IsIgnoreErrorColumns = true;//忽略非数据库列
            db.LogEventStarting = (sql, par) => { Console.WriteLine(sql + " " + par + "\r\n"); };
            return db;
        }

    }
}
