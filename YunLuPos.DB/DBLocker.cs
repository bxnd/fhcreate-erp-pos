﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace YunLuPos.DB
{
    class DBLocker
    {
        private static Mutex locker = new Mutex();

        public static void setLocker()
        {
            locker.WaitOne();
        }

        public static void release()
        {
            locker.ReleaseMutex();
        }
    }
}
