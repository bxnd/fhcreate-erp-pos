﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Entity;
using YunLuPos.Net.DTO;

namespace YunLuPos.Net
{
    public interface MebNetService
    {
        /**
         * 根据商品唯一标识（服务器）查询商品列表
         * */
        Member search(MebSearchInfo info);

        Member query(MebBizBody cardNum);

        Member pay(MebBizBody cardNum);


    }
}
