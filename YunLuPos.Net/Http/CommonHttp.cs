﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.Net.DTO;

namespace YunLuPos.Net.Http
{
    public class CommonHttp:HttpBase, CommonNetService
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(CommonHttp));
        public String heartbeat()
        {
            try
            {
               return doRequest<String>(null, "pos/common/heartbeat");
            }
            catch (NetException ne)
            {
                logger.Error(ne);
                return null;
            }catch(BizError be)
            {
                logger.Error(be);
                return null;
            }
        }

        

        public PosCommInfo sync(PosCommVer ver)
        {
            return doRequest<PosCommInfo>(ver, "pos/common/sync");
        }

        public PosInfo posInfo(RegCode code)
        {
            return doRequest<PosInfo>(code, "pos/common/posinfo");
        }

        public PosInfo regPos(PosInfo posInfo)
        {
            return doRequest<PosInfo>(posInfo, "pos/common/regpos");
        }
    }
}
