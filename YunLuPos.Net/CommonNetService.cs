﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Entity;
using YunLuPos.Net.DTO;
using YunLuPos.Net.Http;

namespace YunLuPos.Net
{
    public interface CommonNetService
    {
        String heartbeat();

        PosCommInfo sync(PosCommVer ver);

        PosInfo posInfo(RegCode code);

        PosInfo regPos(PosInfo posInfo);
    }
}
