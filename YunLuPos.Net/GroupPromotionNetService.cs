﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Entity;
using YunLuPos.Net.DTO;
using YunLuPos.Net.Http;

namespace YunLuPos.Net
{
    public interface GroupPromotionNetService
    {
        List<GroupPromotion> list(PromotionVer maxVersion);

    }
}
