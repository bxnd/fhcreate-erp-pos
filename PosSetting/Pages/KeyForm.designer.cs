﻿namespace PosSetting.Pages

{
    partial class KeyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.commandNameLabel = new System.Windows.Forms.Label();
            this.keycodeLable = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // commandNameLabel
            // 
            this.commandNameLabel.AutoSize = true;
            this.commandNameLabel.Location = new System.Drawing.Point(16, 14);
            this.commandNameLabel.Name = "commandNameLabel";
            this.commandNameLabel.Size = new System.Drawing.Size(29, 12);
            this.commandNameLabel.TabIndex = 8;
            this.commandNameLabel.Text = "操作";
            // 
            // keycodeLable
            // 
            this.keycodeLable.AutoSize = true;
            this.keycodeLable.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.keycodeLable.ForeColor = System.Drawing.Color.Red;
            this.keycodeLable.Location = new System.Drawing.Point(66, 46);
            this.keycodeLable.Name = "keycodeLable";
            this.keycodeLable.Size = new System.Drawing.Size(22, 21);
            this.keycodeLable.TabIndex = 7;
            this.keycodeLable.Text = "K";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "新键值：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(149, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "回车确认 ESC取消";
            // 
            // KeyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(266, 113);
            this.Controls.Add(this.commandNameLabel);
            this.Controls.Add(this.keycodeLable);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "KeyForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "输入新的键值";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyForm_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label commandNameLabel;
        public System.Windows.Forms.Label keycodeLable;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
    }
}