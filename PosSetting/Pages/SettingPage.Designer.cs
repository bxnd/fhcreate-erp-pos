﻿namespace PosSetting.Pages
{
    partial class SettingPage
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button6 = new System.Windows.Forms.Button();
            this.printFileName = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.lptProt = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comCheckBit = new System.Windows.Forms.ComboBox();
            this.comStopBit = new System.Windows.Forms.ComboBox();
            this.comBit = new System.Windows.Forms.ComboBox();
            this.comRate = new System.Windows.Forms.ComboBox();
            this.comPort = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pNone = new System.Windows.Forms.RadioButton();
            this.pFile = new System.Windows.Forms.RadioButton();
            this.pUsb = new System.Windows.Forms.RadioButton();
            this.pLtp = new System.Windows.Forms.RadioButton();
            this.pCom = new System.Windows.Forms.RadioButton();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button6);
            this.groupBox4.Controls.Add(this.printFileName);
            this.groupBox4.Location = new System.Drawing.Point(429, 106);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(385, 218);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "文件输出路径";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(290, 33);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(62, 23);
            this.button6.TabIndex = 10;
            this.button6.Text = "选择";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // printFileName
            // 
            this.printFileName.Location = new System.Drawing.Point(13, 33);
            this.printFileName.Name = "printFileName";
            this.printFileName.Size = new System.Drawing.Size(275, 23);
            this.printFileName.TabIndex = 9;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.lptProt);
            this.groupBox2.Location = new System.Drawing.Point(216, 106);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(206, 218);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "并口设置";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 37);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 14);
            this.label14.TabIndex = 7;
            this.label14.Text = "端口:";
            // 
            // lptProt
            // 
            this.lptProt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lptProt.FormattingEnabled = true;
            this.lptProt.Items.AddRange(new object[] {
            "LPT1",
            "LPT2",
            "LPT3",
            "LPT4",
            "LPT5",
            "LPT6",
            "LPT7"});
            this.lptProt.Location = new System.Drawing.Point(89, 34);
            this.lptProt.Name = "lptProt";
            this.lptProt.Size = new System.Drawing.Size(95, 22);
            this.lptProt.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.comCheckBit);
            this.groupBox1.Controls.Add(this.comStopBit);
            this.groupBox1.Controls.Add(this.comBit);
            this.groupBox1.Controls.Add(this.comRate);
            this.groupBox1.Controls.Add(this.comPort);
            this.groupBox1.Location = new System.Drawing.Point(7, 106);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(205, 218);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "串口设置";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 176);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 14);
            this.label13.TabIndex = 9;
            this.label13.Text = "校验位:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 141);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 14);
            this.label12.TabIndex = 8;
            this.label12.Text = "停止位:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 106);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 14);
            this.label11.TabIndex = 7;
            this.label11.Text = "数据位:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 71);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 14);
            this.label10.TabIndex = 6;
            this.label10.Text = "波特率:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 14);
            this.label9.TabIndex = 5;
            this.label9.Text = "端口:";
            // 
            // comCheckBit
            // 
            this.comCheckBit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comCheckBit.FormattingEnabled = true;
            this.comCheckBit.Items.AddRange(new object[] {
            "None",
            "Odd",
            "Even",
            "Mark",
            "Space"});
            this.comCheckBit.Location = new System.Drawing.Point(84, 173);
            this.comCheckBit.Name = "comCheckBit";
            this.comCheckBit.Size = new System.Drawing.Size(95, 22);
            this.comCheckBit.TabIndex = 4;
            // 
            // comStopBit
            // 
            this.comStopBit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comStopBit.FormattingEnabled = true;
            this.comStopBit.Items.AddRange(new object[] {
            "0",
            "1",
            "1.5",
            "2"});
            this.comStopBit.Location = new System.Drawing.Point(84, 138);
            this.comStopBit.Name = "comStopBit";
            this.comStopBit.Size = new System.Drawing.Size(95, 22);
            this.comStopBit.TabIndex = 3;
            // 
            // comBit
            // 
            this.comBit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comBit.FormattingEnabled = true;
            this.comBit.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.comBit.Location = new System.Drawing.Point(84, 103);
            this.comBit.Name = "comBit";
            this.comBit.Size = new System.Drawing.Size(95, 22);
            this.comBit.TabIndex = 2;
            // 
            // comRate
            // 
            this.comRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comRate.FormattingEnabled = true;
            this.comRate.Items.AddRange(new object[] {
            "300",
            "600",
            "1200",
            "1800",
            "2400",
            "4800",
            "7200",
            "9600",
            "14400",
            "19200",
            "38400",
            "57600",
            "115200",
            "230400",
            "460800",
            "921600"});
            this.comRate.Location = new System.Drawing.Point(84, 68);
            this.comRate.Name = "comRate";
            this.comRate.Size = new System.Drawing.Size(95, 22);
            this.comRate.TabIndex = 1;
            // 
            // comPort
            // 
            this.comPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comPort.FormattingEnabled = true;
            this.comPort.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "COM10",
            "COM11",
            "COM12",
            "COM13",
            "COM14",
            "COM15"});
            this.comPort.Location = new System.Drawing.Point(84, 33);
            this.comPort.Name = "comPort";
            this.comPort.Size = new System.Drawing.Size(95, 22);
            this.comPort.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.pNone);
            this.groupBox3.Controls.Add(this.pFile);
            this.groupBox3.Controls.Add(this.pUsb);
            this.groupBox3.Controls.Add(this.pLtp);
            this.groupBox3.Controls.Add(this.pCom);
            this.groupBox3.Location = new System.Drawing.Point(7, 15);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(804, 68);
            this.groupBox3.TabIndex = 20;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "打印类型";
            // 
            // pNone
            // 
            this.pNone.AutoSize = true;
            this.pNone.Location = new System.Drawing.Point(417, 30);
            this.pNone.Name = "pNone";
            this.pNone.Size = new System.Drawing.Size(67, 18);
            this.pNone.TabIndex = 10;
            this.pNone.TabStop = true;
            this.pNone.Text = "不打印";
            this.pNone.UseVisualStyleBackColor = true;
            // 
            // pFile
            // 
            this.pFile.AutoSize = true;
            this.pFile.Location = new System.Drawing.Point(321, 30);
            this.pFile.Name = "pFile";
            this.pFile.Size = new System.Drawing.Size(53, 18);
            this.pFile.TabIndex = 9;
            this.pFile.TabStop = true;
            this.pFile.Text = "文件";
            this.pFile.UseVisualStyleBackColor = true;
            // 
            // pUsb
            // 
            this.pUsb.AutoSize = true;
            this.pUsb.Location = new System.Drawing.Point(223, 30);
            this.pUsb.Name = "pUsb";
            this.pUsb.Size = new System.Drawing.Size(46, 18);
            this.pUsb.TabIndex = 8;
            this.pUsb.TabStop = true;
            this.pUsb.Text = "USB";
            this.pUsb.UseVisualStyleBackColor = true;
            // 
            // pLtp
            // 
            this.pLtp.AutoSize = true;
            this.pLtp.Location = new System.Drawing.Point(116, 30);
            this.pLtp.Name = "pLtp";
            this.pLtp.Size = new System.Drawing.Size(53, 18);
            this.pLtp.TabIndex = 7;
            this.pLtp.TabStop = true;
            this.pLtp.Text = "并口";
            this.pLtp.UseVisualStyleBackColor = true;
            // 
            // pCom
            // 
            this.pCom.AutoSize = true;
            this.pCom.Location = new System.Drawing.Point(8, 30);
            this.pCom.Name = "pCom";
            this.pCom.Size = new System.Drawing.Size(53, 18);
            this.pCom.TabIndex = 6;
            this.pCom.Text = "串口";
            this.pCom.UseVisualStyleBackColor = true;
            // 
            // SettingPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SettingPage";
            this.Size = new System.Drawing.Size(826, 458);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox printFileName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox lptProt;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comCheckBit;
        private System.Windows.Forms.ComboBox comStopBit;
        private System.Windows.Forms.ComboBox comBit;
        private System.Windows.Forms.ComboBox comRate;
        private System.Windows.Forms.ComboBox comPort;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton pNone;
        private System.Windows.Forms.RadioButton pFile;
        private System.Windows.Forms.RadioButton pUsb;
        private System.Windows.Forms.RadioButton pLtp;
        private System.Windows.Forms.RadioButton pCom;
    }
}
