﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace YunLuPos.Com
{
    public partial class BaseLabel : Label
    {
        public BaseLabel()
        {
            InitializeComponent();
        }

        public BaseLabel(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
    }
}
