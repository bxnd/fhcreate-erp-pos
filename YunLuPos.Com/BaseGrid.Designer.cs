﻿namespace YunLuPos.Com
{
    partial class BaseGrid
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            CustomMainColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            CustomFoceColor = System.Drawing.Color.White;
            this.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.rowPostPaint);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.paint);

            components = new System.ComponentModel.Container();

            this.AutoGenerateColumns = false;


            this.BackgroundColor = CustomMainColor;
            this.GridColor = CustomFoceColor;
            this.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.ReadOnly = true;
            this.MultiSelect = false;
            this.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;

            //表格（列）头部样式
            this.EnableHeadersVisualStyles = false;
            System.Windows.Forms.DataGridViewCellStyle headerStyle = new System.Windows.Forms.DataGridViewCellStyle();
            headerStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            headerStyle.BackColor = CustomMainColor;
            headerStyle.ForeColor = CustomFoceColor;
            headerStyle.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            headerStyle.SelectionBackColor = System.Drawing.SystemColors.Control;
            headerStyle.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            headerStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.EnableHeadersVisualStyles = false;
            this.ColumnHeadersDefaultCellStyle = headerStyle;
            this.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.ColumnHeadersHeight = 25;

            //表格（行）头部样式
            System.Windows.Forms.DataGridViewCellStyle rowHeaderStyle = new System.Windows.Forms.DataGridViewCellStyle();
            rowHeaderStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            rowHeaderStyle.BackColor = CustomMainColor;
            rowHeaderStyle.ForeColor = CustomFoceColor;
            rowHeaderStyle.SelectionBackColor = CustomFoceColor;
            rowHeaderStyle.SelectionForeColor = CustomMainColor;
            rowHeaderStyle.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            rowHeaderStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.RowHeadersDefaultCellStyle = rowHeaderStyle;
            this.RowTemplate.Height = 25;
            this.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;

            //单元格样式
            System.Windows.Forms.DataGridViewCellStyle cellStyle = new System.Windows.Forms.DataGridViewCellStyle();
            cellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            cellStyle.BackColor = CustomMainColor;
            cellStyle.ForeColor = CustomFoceColor;
            cellStyle.SelectionBackColor = CustomFoceColor;
            cellStyle.SelectionForeColor = CustomMainColor;
            cellStyle.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            cellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DefaultCellStyle = cellStyle;
        }

        #endregion
    }
}
