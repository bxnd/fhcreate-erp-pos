﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YunLuPos.Com
{
    public partial class BaseTextInput : TextBox
    {
        public BaseTextInput()
        {
            InitializeComponent();
        }

        public BaseTextInput(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
    }
}
