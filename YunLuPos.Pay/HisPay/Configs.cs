﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HisPay.Payment
{
    class Configs
    {
        public static Boolean isLoaded = false;
        public static String MCH_ID = "";

        public static String SHOP_CODE = "";

        public static String DEVICE_INFO = "";

        public static String OPERATOR_ID = "";

        public static String SERVER_URL = "";

        public static String BODY = "";

        public static int CONN_TIMEOUT = 15000;

        public static int READ_TIMEOUT = 60000;

        /**
         * 初始化系统配置文件
         * @param path
         */
        public static void initConfig(String path)
        {
            Logger.debug("初始化配置信息");
            List<String> lines = new List<String>();
            FileStream fs = null;
            StreamReader sr = null;
            String line = "";
            try
            {
                fs = new FileStream(path, FileMode.Open, FileAccess.Read);
                sr = new StreamReader(fs,Encoding.Default);
                while ((line = sr.ReadLine()) != null)
                {
                    lines.Add(line);
                }
            }
            catch (Exception e)
            {
                Logger.debug(e);
                throw ClientException.READ_CONFIG;
            }
            finally
            {
                if (fs != null)
                    fs.Close();
                if (sr != null)
                    sr.Close();
            }
            foreach (String str in lines)
            {
                if (str != null && !"".Equals(str) && !str.StartsWith("#"))
                {
                    String[] arr = str.Split('=');
                    if (arr.Length == 2 && !"".Equals(arr[0]) && !"".Equals(arr[1]))
                    {
                        String key = arr[0].Trim();
                        String v = arr[1].Trim();
                        if ("MCH_ID".Equals(key))
                        {
                            MCH_ID = v;
                        }
                        else if ("DEVICE_INFO".Equals(key))
                        {
                            DEVICE_INFO = v;
                        }
                        else if ("SHOP_CODE".Equals(key))
                        {
                            SHOP_CODE = v;
                        }
                        else if ("OPERATOR_ID".Equals(key))
                        {
                            OPERATOR_ID = v;
                        }
                        else if ("SERVER_URL".Equals(key))
                        {
                            SERVER_URL = v;
                        }
                        else if ("BODY".Equals(key))
                        {
                            BODY = v;
                        }
                        else if ("CONN_TIMEOUT".Equals(key))
                        {
                            try
                            {
                                CONN_TIMEOUT = Int32.Parse(v);
                            }
                            catch (Exception e)
                            {
                                Logger.debug(e);
                                throw new ClientException("INVALID_CONFIG_VALUE", "连接超时时间");
                            }
                        }
                        else if ("READ_TIMEOUT".Equals(key))
                        {
                            try
                            {
                                READ_TIMEOUT = Int32.Parse(v);
                            }
                            catch (Exception e)
                            {
                                Logger.debug(e);
                                throw new ClientException("INVALID_CONFIG_VALUE", "获取服务器数据超时时间");
                            }
                        }
                    }
                }
            }
            checkConfig();
        }

        static void checkConfig()
        {
            Logger.debug("检查配置信息");
            String configName = "";
            if ("".Equals(MCH_ID))
            {
                configName = "商户号[MCH_ID]";
            }
            else if ("".Equals(DEVICE_INFO))
            {
                configName = "机器号[DEVICE_INFO]";
            }
            else if ("".Equals(SERVER_URL))
            {
                configName = "服务器地址[SERVER_URL]";
            }
            else if ("".Equals(BODY))
            {
                configName = "订单描述[BODY]";
            }
            else if ("".Equals(SHOP_CODE))
            {
                configName = "门店编号[SHOP_CODE]";
            }
            if (!"".Equals(configName))
            {
                throw new ClientException("MISS_CONFIG", "配置缺失-" + configName);
            }
            else
            {
                isLoaded = true;
            }
        }
    }
}
