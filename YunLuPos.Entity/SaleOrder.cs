﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Entity
{
    public class SaleOrder
    {
        
        public Int64 id {get;set;}

        public string cliqueCode {get;set;}

        public string orderId {get;set;}

        public string orderCode {get;set;}

        public string branchCode {get;set;}

        public string posCode {get;set;}

        public string cashierCode {get;set;}

        public string cashierName {get;set;}

        public Double realAmount {get;set;}

        public Double payAmount {get;set;}

        public Double disAmount {get;set;}

        public Double dotAmount {get;set;}

        public Double goodsCount {get;set;}

        public string orderType {get;set;}

        public string state {get;set;}

        public string memberNo {get;set;}

        public string memberName {get;set;}

        public string saleDate {get;set;}

        public string createTime {get;set;}

        public string doneTime {get;set;}

        public List<SaleOrderGoods> goods { get; set; }

        public List<SaleOrderAccount> accounts { get; set; }
    }
}
