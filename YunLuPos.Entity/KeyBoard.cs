﻿using System;
using System.Linq;
using System.Text;

namespace YunLuPos.Entity
{
    public class KeyBoard
    {
        
        public Int64 id {get;set;}

        public string keyCode {get;set;}

        public string commandKey {get;set;}

        public string commandName {get;set;}

        public string holder {get;set;}

        public string showTouch {get;set;}

        public string isEable { get; set; }

        public Int64 seq {get;set;}

        public String toString()
        {
            return keyCode + "," + commandKey + "," + commandName + "," + holder + "," + showTouch + "," + isEable+","+seq;
        }

    }
}
