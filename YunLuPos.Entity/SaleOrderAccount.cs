﻿using System;
using System.Linq;
using System.Text;

namespace YunLuPos.Entity
{
    public class SaleOrderAccount
    {
        
        public Int64 id {get;set;}

        public string cliqueCode {get;set;}

        public string branchCode {get;set;}

        public string orderId {get;set;}

        public string orderCode {get;set;}

        public Double amount {get;set;}

        public string typeKey {get;set;}

        public String masterTypeKey { get; set; }

        public string typeName { get; set; }

        public string trandNo {get;set;}

    }
}
