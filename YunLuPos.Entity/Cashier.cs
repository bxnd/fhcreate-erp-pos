﻿using System;
using System.Linq;
using System.Text;

namespace YunLuPos.Entity
{
    public class Cashier
    {
        
        public Int64 id {get;set;}

        public string cliqueCode { get; set; }

        public string branchCode { get; set; }

        public string cashierCode {get;set;}

        public string cashierName {get;set;}

        public string pwd {get;set;}

        public string authString {get;set;}

        public string status { get; set; }

        public Int64 ver { get; set; }



    }
}
