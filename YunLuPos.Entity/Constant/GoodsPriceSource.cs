﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Entity.Constant
{
    public enum GoodsPriceSource
    {
        NORM,
        PROM,
        GROUP_PROM,
        VIP,
        POS,
        BALANCE
    }
}
